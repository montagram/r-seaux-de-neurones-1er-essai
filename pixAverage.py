from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import bpnn2hidden

NB_CER_IM = 42
NB_OTH_IM = 32
SIZE = 10

data = []

def matrixToHorizontalAverageArray(matrix):
	#this returns an array of the row elements sumed (the 1 stands for horizontal sum)
	matSum = np.sum(matrix, 1)

	size = len(matSum)
	#print size, "horsize"
	result = []

	for x in matSum:
		#print x, "x"
		result.append(int(x/size))

	#print result
	return result

def matrixToVerticalAverageArray(matrix):
	matSum = np.sum(matrix, 0)

	size = len(matSum)
	#print size, "versize"
	result = []

	for y in matSum:
		#print y, "y"
		result.append(int(y/size))

	#print result
	return result

def loadIm(nbCerImages, nbOthImages):
	data = []

	for cervinImages in xrange(nbCerImages):
		dataBuf = []

		im = Image.open(open("Im/MC/mc" + str(nbCerImages + 1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((SIZE,SIZE))

		matrix = np.asarray(im)

		vertAv = matrixToVerticalAverageArray(matrix)
		horiAv = matrixToHorizontalAverageArray(matrix)

		dataBuf.append(vertAv + horiAv)
		dataBuf.append([1])

		data.append(dataBuf)

	for otherImages in xrange(nbOthImages):
		dataBuf = []

		im = Image.open(open("Im/PM/" + str(nbOthImages+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((SIZE,SIZE))

		matrix = np.asarray(im)

		vertAv = matrixToVerticalAverageArray(matrix)
		horiAv = matrixToHorizontalAverageArray(matrix)

		dataBuf.append(vertAv + horiAv)
		dataBuf.append([0])

		data.append(dataBuf)

	#print data
	return data

def main(nbCerImages = NB_CER_IM, nbOthImages = NB_OTH_IM):
	matrix = loadIm(nbCerImages, nbOthImages)

	n = bpnn2hidden.NN(2*SIZE, 13, 3, 1)

	n.train(matrix)

	n.test(matrix)


if __name__ == '__main__':
    main()


