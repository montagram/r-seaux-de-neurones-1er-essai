Photos a récupérer sur le drive dans Phase2/Photo.

Dans un premier temps on utilise une seule face du mont cervin MC_face1
et les photos random sans autre montagne PM_sansmontagne.

Les réseaux de neurones sont implementés dans bpnn.
bpnn - une hidden layer
bpnn2hidden - deux hidden layer
bpnn3hidden - trois hidden layer

Le fichier a lancé est testImag.py

----------------------------------------------------------------------------------

Files wich start with bpnn are neural network implementations, only the number of hidden layers changes.

bpnn2hidden have two exclusive training methods named trainDelta & trainDeltaMuet, they booth use the fact that the error e converges when a certain number of iterations have been done.

testImag.py uses all the pixels's grey level of an image as input whereas pisAvUserFriendly.py uses the vertical and horizontal average of this levels as input.

the mainPrintingInfo function can be use to see which neural network architecture gives better result with a given input size and database.
