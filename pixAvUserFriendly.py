from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import bpnn2hidden

NB_CER_IM = 42
NB_OTH_IM = 32
SIZE = 30

data = []

def matrixToHorizontalAverageArray(matrix):
	#this returns an array of the row elements sumed (the 1 stands for horizontal sum)
	matSum = np.sum(matrix, 1)

	size = len(matSum)
	#print size, "horsize"
	result = []

	for x in matSum:
		#print x, "x"
		result.append(int(x/size))

	#print result
	return result

def matrixToVerticalAverageArray(matrix):
	matSum = np.sum(matrix, 0)

	size = len(matSum)
	#print size, "versize"
	result = []

	for y in matSum:
		#print y, "y"
		result.append(int(y/size))

	#print result
	return result

def loadIm(nbCerImages, nbOthImages):
	data = []

	for cervinImages in xrange(nbCerImages):
		dataBuf = []

		im = Image.open(open("Im/MC/mc" + str(cervinImages + 1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((SIZE,SIZE))

		matrix = np.asarray(im)

		vertAv = matrixToVerticalAverageArray(matrix)
		horiAv = matrixToHorizontalAverageArray(matrix)

		dataBuf.append(vertAv + horiAv)
		dataBuf.append([1])

		data.append(dataBuf)

	for otherImages in xrange(nbOthImages):
		dataBuf = []

		im = Image.open(open("Im/PM/" + str(otherImages+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((SIZE,SIZE))

		matrix = np.asarray(im)

		vertAv = matrixToVerticalAverageArray(matrix)
		horiAv = matrixToHorizontalAverageArray(matrix)

		dataBuf.append(vertAv + horiAv)
		dataBuf.append([0])

		data.append(dataBuf)

	#print data
	return data

def loadInput(name):
	data = []

	im = Image.open(open(name + ".jpg", 'rb'))
	im = im.convert("L")
	im = im.resize((SIZE,SIZE))

	matrix = np.asarray(im)

	vertAv = matrixToVerticalAverageArray(matrix)
	horiAv = matrixToHorizontalAverageArray(matrix)

	data.append(vertAv + horiAv)

	return data

def test(n):
	while (1):
		Im = input ("Quel est le nom du fichier jpg?")
		print "vous avez ecrit", Im
		matTest = loadInput(Im)
		matTest.append([1])
		print matTest
		n.test([matTest])
		pass

def mainAskingImagesToTest(nbCerImages = NB_CER_IM, nbOthImages = NB_OTH_IM):
	matrix = loadIm(nbCerImages, nbOthImages)

	print ("nb de coef pour h1? (h1 doit etre < a ", SIZE)
	nh1 = input ()
	print ("nb de coef pour h2? (h2 doit etre <= a ", nh1)
	nh2 = input ()

	n = bpnn2hidden.NN(2*SIZE, nh1, nh2, 1)

	e = n.trainDelta(matrix)

	print (SIZE, nh1, nh2, e)

	test(n)

def mainPrintingInfos(nbCerImages = NB_CER_IM, nbOthImages = NB_OTH_IM):
	matrix = loadIm(nbCerImages, nbOthImages)

	mini = 100.0

	for i in xrange(SIZE):
		for j in xrange(i):

			n = bpnn2hidden.NN(2*SIZE, i+1, j+1, 1)
			e = n.trainDeltaMuet(matrix)
			print (i,j)
			print (SIZE, i + 1, j + 1, e)

			if (mini > e):
				mini = e
			pass
		pass

	print mini


if __name__ == '__main__':
    #mainAskingImagesToTest()
    mainPrintingInfos()