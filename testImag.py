from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import bpnn
import bpnn2hidden
import bpnn3hidden

TAILLE = 100
NB_IM_CER = 43
NB_IM_OTHERS = 32

data = []

np.set_printoptions(threshold='nan')

def loadImCervin(data, nbImages = NB_IM_CER):
	print nbImages

	for imageCervin in xrange(nbImages):

		datBuf = []
		
		im = Image.open(open("Im/MC/mc" + str(imageCervin+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((TAILLE,TAILLE))
		#im.show()
		#print (imageCervin)
		matrix = np.asarray(im)
		imVect = matrix.flatten().tolist()
		#print imVect
		datBuf.append(imVect)
		size = len(imVect)
		#print datBuf
		datBuf.append([1])
		#print datBuf
		data.append(datBuf)
	return [data, size]	

def loadImOthers(data, nbImages = NB_IM_OTHERS):

	for imageAutre in xrange(nbImages):

		datBuf = []

		im = Image.open(open("Im/PM/" + str(imageAutre+1) + ".jpg", 'rb'))
		im = im.convert("L")
		im = im.resize((TAILLE,TAILLE))
		#im.show()
		matrix = np.asarray(im)
		imVect = matrix.flatten().tolist()
		#print imVect
		datBuf.append(imVect)
		size = len(imVect)
		#print datBuf
		datBuf.append([0])
		#print datBuf
		data.append(datBuf)
	return [data, size]

def main(data, nbHiddenCoef):

	TabCervin = loadImCervin(data)
	TabOthers = loadImOthers(data)

	size = TabOthers[1]
	data = TabOthers[0]

	#print data

	n = bpnn.NN(size, nbHiddenCoef, 1)

	n.train(data)
	n.test(data)

def main2(data, nbHiddenCoef):

	TabCervin = loadImCervin(data)
	TabOthers = loadImOthers(data)

	size = TabOthers[1]
	data = TabOthers[0]
	
	n = bpnn2hidden.NN(size, nbHiddenCoef, nbHiddenCoef, 1)

	n.train(data)

	n.test(data)

def main3(data, nbHiddenCoef):

	TabCervin = loadImCervin(data)
	TabOthers = loadImOthers(data)

	size = TabOthers[1]
	data = TabOthers[0]

	n = bpnn3hidden.NN(size, nbHiddenCoef, nbHiddenCoef, nbHiddenCoef, 1)

	n.train(data)

	n.test(data)

if __name__ == '__main__':
	#main3(data, 10)
	main2(data, 10)
	#main(data, 10)



